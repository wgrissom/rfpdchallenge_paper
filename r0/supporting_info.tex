\documentclass[11pt]{article}
% Command line: pdflatex -shell-escape LT_Pulse.tex
\usepackage{geometry} 
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{times}
\usepackage{bm}
\usepackage{fixltx2e}
\usepackage[outerbars]{changebar}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{color}
\usepackage{tabularx}

\usepackage{dialogue} % to typeset interviews

\epstopdfsetup{suffix=} % to remove 'eps-to-pdf' suffix from converted images
%\usepackage{todonotes} % use option disable to hide all comments

\usepackage[sort&compress]{natbib}
\bibpunct{[}{]}{,}{n}{,}{,}

\usepackage[noend]{algpseudocode}

\usepackage{dsfont}
\usepackage{relsize}

%changing the Eq. tag to use [] when numbering. use \eqref{label} to reference equations in text.
\makeatletter
  \def\tagform@#1{\maketag@@@{[#1]\@@italiccorr}}
\makeatother

\linespread{1}
%\setlength{\parindent}{0in}

% the following command can be used to mark changes made due to reviewers' concerns. Use as \revbox{Rx.y} for reviewer x, concern y.
\newcommand{\revbox}[1]{\marginpar{\framebox{#1}}}
%\newcommand{\revbox}[1]{}

%\newcommand{\bop}{$\vert B_1^+ \vert$}
\newcommand{\kt}{$k_\textrm{T}$}
\newcommand{\bone}{$B_1^+$}
\newcommand{\bmap}{$B_1^+$}
\newcommand{\mytilde}{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}  % tilde symbol
\mathchardef\mhyphen="2D

\begin{document}



\newpage
  
\title{{\bf Supporting Information:} Pushing the envelope in RF pulse design using an open-competition format: Report from the 2015 ISMRM Challenge}
\author{William A. Grissom$^{*1,2}$, Kawin Setsompop$^{3}$, Samuel A. Hurley$^{4}$, Jeffrey Tsao$^5$, \\Julia V. Velikina$^6$, Alexey A. Samsonov$^7$}
\maketitle
\begin{flushleft}
$^1$Vanderbilt University Institute of Imaging Science, Nashville, Tennessee, USA\\
$^2$Department of Biomedical Engineering, Vanderbilt University, Nashville, Tennessee, USA\\
$^3$Athinoula A. Martinos Center for Biomedical Imaging, Massachusetts General Hospital, Harvard Medical School, Boston, Massachusetts, USA\\
$^4$Synaptive Medical, Toronto, Ontario, Canada\\
$^5$McKinsey \& Company, Boston, Massachusetts, USA\\
$^6$Department of Medical Physics, University of Wisconsin, Madison, USA\\
$^7$Department of Radiology, University of Wisconsin, Madison, USA 

\par
-------------------------- 

\par
*Corresponding author: \\
Will Grissom\\
Department of Biomedical Engineering\\
Vanderbilt University\\
5824 Stevenson Center\\
Nashville, TN 37235 USA \\
E-mail: will.grissom@vanderbilt.edu \\
Twitter: @wgrissom

\end{flushleft}
\thispagestyle{plain}

\pagebreak

%%%%%%%%%%%%%%%%%%%%%%% Abstract %%%%%%%%%%%%%%%%%%%%%%%


\paragraph{Interview with Mihir Pendse of team StanfordUHF}
\begin{dialogue}
\speak{Q} What motivated you to participate in the Challenge?
\speak{A} I think this challenge was a good way to compare different pulse design ideas across institutions in a well-organized manner. 
For me it was a way to assess my own skill in RF pulse design.
\speak{Q} How hard was it to get started? 
\speak{A} It wasn't hard to get started as the problem description was defined very clearly by the organizers. 
The challenge input data was slightly different than the inputs my existing code took, 
so some small code modifications were necessary.
\speak{Q} Tell us about the design approach you devised, at a high level. 
%Did you connect various existing methods like building blocks, or come up with a wholly new approach?
\speak{A} The challenge required optimizing the slice profile (or SMS profile),
the channel weightings and the spokes locations for in-slice homogeneity. 
I was able to design the single-slice torso pulses using a straightforward adaptation of my previous
minimum-SAR spokes design method (IMPULSE) \cite{pendse:ismrm15:impulse},
but the brain SMS designs required me to develop a new approach 
in which I optimized the subpulse shape for each slice so that the summed power
would be minimized, using an optimal control approach adapted from the method of Aigner et al \cite{aigner:jmr:2016}. 
After that I further adjusted the phase of each slice's subpulse to minimize the peak amplitude of the summed subpulses, and applied time-optimal variable-rate selective excitation (VERSE) \cite{Conolly:1988:JMR,lee2009tod}. 
%My design approach mostly involved taking concepts from the pTx literature, 
%the SMS literature, and general RF pulse design techniques and integrating them. 
%I would not say that I came up with a completely new approach.
%Fairly straightforward application of previous methods for single-slice spokes but for SMS it required some new developments. Optimized the subpulse shape for each slice so that the summed peak power would be minimum, using optimal control theory. Afterwards adjusted the phases for each subpulse so that sum would be minimum. Applied VERSE to make sure peak amplitude constraint was met. Used something similar to Aigner’s approach. Spokes-based. Corkscrew trajectory for RF pulses. ISMRM abstract from 2015, p. 543. This year, 1017.
\speak{Q} Did your approach evolve during the Challenge as you found ways to improve it, or did it stay fixed and you found improvements through manual adjustments?
\speak{A} The approach evolved significantly during the course of the challenge especially for the pTx-SMS tasks as I tried to come up with ways to reduce the peak pulse power in a time-optimal manner.
One of the ideas I implemented was to reduce the subpulse durations 
using minimum rather than linear phase pulses, and I found I was still able to  
keep the phase roll through the slice flat enough when I did that.
\speak{Q} What did you learn from the Challenge, at a high level? 
\speak{A} While I was fairly comfortable with pTx theory before the challenge, 
I learned a lot about other aspects of RF pulse design including the design of SMS pulses.
\speak{Q} If you were an organizer of the Challenge, what would you have done differently? 
\speak{A} While the challenge was well organized I think there were several limitations in the design specifications: 
\begin{enumerate}
\item The design tasks were limited to the small tip regime which made the problem considerably simpler. 
I think the skills of the contestants would have really been tested with more demanding large-tip design tasks.
\item There was no $B_0$ inhomogeneity incorporated into the pulse design so the $B_0$ robustness of the pulses was not assessed. 
One of the limitations of the spokes trajectory is it is not very robust to $B_0$ inhomogeneity so I think my design may be different if off-resonance was a concern, even though the durations were relatively short. 
This concern is abated by the fact that most of my designs were very short, 
through my coronal SMS pulse (Figure 4) was about 5 ms long, 
so $B_0$ considerations may have changed my solution.
\item SAR information was provided to the contestants only in the form of VOPs. 
If the SAR matrices were provided directly, 
that would provide more ability for the contestants to make better use of SAR headroom and perhaps achieve a better pulse.
\item There was no dwell time specification and some of the dwell times that I used were much shorter than what is practical on a scanner. 
Maybe a lower limit on the dwell time should have been specified.
\end{enumerate}
\speak{Q} What's next? Did you come up with new ideas that you will further develop and publish, as a result of your participation in the Challenge?
\speak{A} Yes, I am in the process of preparing a manuscript on my IMPULSE pTx design methods that I used and extended for the challenge.
That manuscript will include the new SMS-pTx methods that I developed for the challenge,
which in particular address improved subpulse shapes. 
I presented early accounts of this work at the 2015 and 2016 ISMRM meetings \cite{pendse:ismrm15:impulse,pendse:ismrm16:impulsesms}. %Is planning to publish an SMS-pTx paper, will have advanced due to challenge, especially wrt improving subpulse shapes.
\speak{Q} What do you think was unique about your approach that gave you an edge
over other contestants? 
\speak{A} I think the ability of my IMPULSE method to mitigate local SAR hotspots is superior to existing methods in the literature as it exploits optimization of both spokes locations and channel weightings even though this makes the problem nonconvex. 
The ADMM algorithm used in IMPULSE is particularly efficient and even allows for optimization using SAR matrices directly without VOP compression, 
although that wasn't relevant for this challenge. 
I think formulating the pTx optimization as minimizing SAR subject to in-plane inhomogeneity constraints rather than minimizing inhomegeneity subject to absolute SAR constraints (as is more typical in literature) is also advantageous,
because instead of needing to specify the slice selective subpulse shape up front (in order to compute the absolute SAR constraint) 
the slice selective subpulse can be optimized later (after the RF shims and spokes locations have been found) 
in order to minimize total pulse duration subject to absolute hardware, 
SAR, and excitation accuracy constraints.
 
\end{dialogue}

\paragraph{Interview with Armin Rund and Christoph Aigner of team rfcontrol}
\begin{dialogue}
\speak{Q} What motivated you to participate in the Challenge?
\speak{A} Our main motivation was the chance to put our optimal control methods in competition with other approaches, 
and to test our algorithms on accepted problems.

\speak{Q} How hard was it to get started? 
\speak{A} The phase I example code showed that we needed to do a lot. 
We needed to develop new algorithms; in particular we had to develop a new algorithm for time-optimal control \cite{Conolly:1986:Trans-Med-Imag} multiband pulse design to obtain the shortest possible pulses,
and we needed a global optimization scheme to overcome the non-convexity of the problems.
We had done previous work on time-optimal control \cite{KunischPieperRund_2016}, 
which helped us get started.
We also had to develop techniques to incorporate the $l_\infty$-norm constraints that were enforced in the challenge, 
whereas in our previous work we had only used quadratic regularization \cite{aigner:jmr:2016}.

\speak{Q} Tell us about the design approach you devised, at a high level. 
%Did you connect various existing methods like building blocks, or come up with a wholly new approach?
\speak{A} We came up with a new approach that was built using our existing building-block optimization codes that implement semi-smooth Newton and quasi-Newton methods. 
As mentioned, we implemented a time-optimal control design, 
along with a new strategy for globalization based on an auxiliary optimization problem. 
We implemented $l_\infty$-norm constraints on refocusing efficiency error, 
RF amplitude, and the gradient amplitude and slew rate. 
%Finding the global minimizer is impossible, and the rules of challenge were to find the best minimizer. 
%For the setting of a time optimal control with state constraints. 
%We have a new strategy for globalization for time-optimal with l-infinity constraints. 
We also found that using complex RF modulation did not significantly reduce our pulse durations,
so all our designs were real-valued.

\speak{Q} Did your approach evolve during the Challenge as you found ways to improve it, or did it stay fixed and you found improvements through manual adjustments?
\speak{A} We recognized early on that we should use a time-optimal control approach, 
but it evolved quite a lot, particularly in phase I as we tested different sub-algorithms. 
Of course we also made a lot of adjustments to the algorithm parameters along the way. 
%At the end of phase I we added optimization and parallelization to the code to cope with larger problem sets. 
%So it evolved a lot in terms of mechanisms up to end of phase I, and then 
We had to put our code on a diet for phase II, by optimizing and parallelizing our code to cope with the larger number of design
problems in phase II. 
The overall algorithm is structured so that the longer you let it run, the shorter the pulse gets. 
Early in the challenge we let it run for weeks; by the end of phase II we could get the same answer in a day using our optimized codes.

\speak{Q} What did you learn from the Challenge, at a high level? 
\speak{A} The biggest thing we learned is that our collaboration works. 
Armin has an applied math background, and Christoph is working on a challenging application in biomedical engineering that can benefit from Armin's knowledge. 
%We learned that it is quite important to have intuition in order to efficiently handle so many constraints, 
%such as peak RF and gradient slew rates.
%We also found that it was important to develop automated ways to adapt the algorithm parameters for different problems, which will be useful to the broader community if our approaches are used by non-experts.
We also learned how to efficiently handle so many constraints in our optimizations, 
and we found that it is important to adapt algorithm parameters automatically during the iterative optimization, 
which will be useful to the broader community if our code is used as a black box solver. 
%developed algorithms to automatically adapt parameters, beacuse they expected to need to submit the code, but it still helped since there were so many cases.

\speak{Q} If you were an organizer of the Challenge, what would you have done differently? 
\speak{A} It would have been nice to have a dedicated scientific or poster session during the ISMRM meeting in Singapore, to give contestants a forum to present and share their ideas and approaches. 
The whole society could benefit from such an event to connect participants and exchange ideas.

\speak{Q} What's next? Did you come up with new ideas that you will further develop and publish, as a result of your participation in the Challenge?
\speak{A} Our next step is to publish a paper on our constrained and globalized time-optimal control method, 
and to present the new techniques we developed for the challenge such as the globalization steps. 
We may start new collaborations and address more complex questions such as designing more robust inversion pulses or use more complete MR equations such as Bloch-McConnell.
We would also like to try initializing our algorithm with existing low-peak power complex-valued solutions.

\speak{Q} What do you think was unique about your approach that gave you an edge
over other contestants? 
\speak{A} The detailed modeling of the problem set as constrained optimal control problem and its accurate
solution using iterative mathematical optimization techniques allowed us to reduce the pulse duration maximally 
by fully exploiting the constraints. 
The new globalization techniques we developed also helped to improve our scores. 

\end{dialogue}

\bibliographystyle{cse}
\bibliography{rfpd_challenge}

\end{document}
